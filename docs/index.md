# Canadian Pomological Society  

:  
    ![exemplarpie](gallery/2016/pieexemplarwithtransparency-small.png)  


## {++Excellence in apple pies and all things++}


The **Canadian Pomological Society**, originally founded in 1984 by Frank Applebaum and his cohort, holds an annual apple pie bake off and Pommeclave to support good times and excellence in apple pies.


Enter a pie, maybe win a trophy!  
Anyone can submit a pie and enter the annual apple pie bake off.  
***Newcomers encouraged!***

Enter a pie in this year's contest and your name will be etched forever into the annals of glory!

---

# Join us this year!
## Competition and Judging 2019

We hope you will join us at this year's Pommeclave in Toronto!  

### If we can contact you, we can invite you!
## [Please **Click Here** <span class="icomoon-pomological" style="font-size: 10rem;"></span> to join the Pomological Society](./contact/)
