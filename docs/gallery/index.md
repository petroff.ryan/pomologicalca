# Gallery

The gallery is under construction at the moment.  

Please get in touch if you've got pictures that belong here!  


# 2016  

## The Spread  
![thespread](2016/thespread2016transparent.png)



# Swag throughout the years

Many years, we make shirts or other swag, to mark the occasion and give us something we can sport with pride all year.  
Here are some of the surviving articles we were able to document, organized by year. Enjoy!  

### 1986  

![1986 shirt front](swag/swag-shirt-front-1986-small.png)   
Historical!  

### 1988  

![1988 shirt front](swag/swag-shirt-front-1988-small.png)  
This refers to the [1988 Winter Olympics](https://en.wikipedia.org/wiki/1988_Winter_Olympics) held in Calgary.  
The official motto of the games that year was: `"Can You Feel It?"`    

### 1990

![1990 shirt front](swag/swag-shirt-front-6th-1990-small.png)  
This year's swag was a sweatshirt, and a lovely shade of pink!  


### 1991

![1991 shirt front](swag/swag-shirt-front-1991-small.png)  
Do we see the ["Arms of Her Majesty The Queen in Right of Canada"](https://en.wikipedia.org/wiki/Arms_of_Canada) there?    
One day may we finally liberate the Crown's enslaved unicorn, and all of Canada from the monarchy.     


### 1992

![1992 shirt front](swag/swag-shirt-front-1992-small.png)  
The 1992 shirt. Cool! Happy Canada 125, 1992!   

### 1993

![1993 shirt front](swag/swag-shirt-front-1993-small.png)  
This 1993 shirt has the image of a postage stamp validated with a postmark.  
Tasty pies signed, sealed, delivered!  


### 1995

![1995 shirt front](swag/swag-shirt-front-1995-small.png)  
![1995 shirt back](swag/swag-shirt-back-1995-small.png)  
The 1995 shirt, with a small circle on the front breast pocket position, and a larger triangle on the back. Delightful.   

### 1996  

![1996 shirt front](swag/swag-shirt-front-1996-small.png)  
Using 'CPS' as a short form didn't catch on, probably due to the use of 'CPS' by... ['CPS'](https://en.wikipedia.org/wiki/CPS)    

### 1997

![1997 shirt front](swag/swag-shirt-front-1997-small.png)  
![1997 shirt back](swag/swag-shirt-back-1997-small.png)  
"What is Mackintosh?"  

### 1998

![1998 shirt front](swag/swag-shirt-front-1998-small.png)    
Pommeclave!  

### 1999

![1999 shirt front](swag/swag-shirt-front-1999-small.png)  
![1999 shirt back](swag/swag-shirt-back-1999-small.png)  
The last pie bake-off of the *millenium!*  

### 2000

![2000 shirt front](swag/swag-shirt-front-2000-small.png)  
This was one of the classic meetings held on Manitoulin Island.    

### 2001

![2001 shirt front](swag/swag-shirt-front-2001-small.png)  
This is the infamous "Pie Guy" shirt of 2001.  

### 2002  

![18th shirt 2002](swag/swag-shirt-18th-2002-small.png)  
This was the tshirt design from 2002, the 18th annual Pommeclave. Fun fact: The designer was Ryan Petroff, paid in 'high school volunteer hours'.  

### 2003  

![2003 shirt front](swag/swag-shirt-front-2003-small.png)  
Beautiful!  

### 2004

![2004 shirt front](swag/swag-shirt-front-2004.-small.png)  
Another Manitoulin Island year.   

### 2005
![21st shirt 2005](swag/swag-shirt-21st-2005-small.png)  
This year on a black tshirt. Mixing it up!  

### 2011
![2011 shirt front](swag/swag-shirt-front-2011-small.png)  
![2011 shirt back](swag/swag-shirt-back-2011-small.png)    
With the two-glyph "apple pie" on the front and the seal on the back, on striking red fabric.  
This has been our most popular shirt by far, with a visual pun that even normies on the street can put together.  
It's extemely common while wearing this design to hear people yell out "Apple Pie!" with an air of triumph after taking a few steps to figure it out. Instant classic.  

### 2012
![28th hat](swag/swag-hat-28th-small.png)  
This hat was the swag at the 28th Annual Pommeclave, and is a proud daily-wear item.  


### 2014

![2014 shirt front](swag/swag-shirt-front-2014-small.png)  
Thirty years, wow! That's practically a lifetime!  

### 2017

![2017 shirt front](swag/swag-shirt-front-2017-small.png)   
"There's an app(le) for that"  
