# [Pomological.ca](http://pomological.ca)

This holds the build tools configurations and content to create and host Pomological.ca,  
the site of the Canadian Pomological Society,  
to support the yearly Pommeclave.



Using [MkDocs](https://www.mkdocs.org/) to generate the site with [mkdocs-material](https://squidfunk.github.io/mkdocs-material/) to make it look snazzy.  
